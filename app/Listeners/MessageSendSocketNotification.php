<?php

namespace App\Listeners;

use App\Events\MessageSendSocket;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MessageSendSocketNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MessageSendSocket  $event
     * @return void
     */
    public function handle(MessageSendSocket $event)
    {
        //
    }
}
