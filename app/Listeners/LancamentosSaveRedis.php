<?php

namespace App\Listeners;

use App\Events\LancamentosSend;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LancamentosSaveRedis
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LancamentosSend  $event
     * @return void
     */
    public function handle(LancamentosSend $event)
    {
        //
    }
}
