<?php

namespace App\Http\Controllers\Lancamento;

use App\Model\Lancamento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

class LancamentosController extends Controller
{

    public function lists()
    {
        $registros = collect();
        foreach (\Redis::keys( Lancamento::REDIS_KEY.':*') as $keys){
            $registros->push(json_decode(Redis::get($keys)));
        }
        return view('show')->with('registros', $registros);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * [
            'descricao' => 'Salário Lojacorr',
            'categoria' => 'Salário',
            'data_vencimento' => date('Y-m-d'),
            'data_pagamento'  => date('Y-m-d H:i:s'),
            'valor' => 3000.00,
            'tipo_lancamento' => 2,
            'quitada' => 1
        ]
     *
     *
     */
    public function store(Request $request)
    {

        $validation = validator(
            $request->all(),
            [
                'descricao' => 'required',
                'categoria' => 'required',
                'data_vencimento' => 'required|date',
                'data_pagamento'  => 'required|date',
                'valor' => 'required|numeric',
                'tipo_lancamento' => 'required|integer',
                'quitada' => 'required|integer',
            ]
        );

        $this->lancamento = new Lancamento($request->all());
        $keys  = \Redis::keys( Lancamento::REDIS_KEY.':*');
        $total = count($keys) + 1;
        \Redis::set(Lancamento::REDIS_KEY.':'.$total, $this->lancamento->toJson());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
