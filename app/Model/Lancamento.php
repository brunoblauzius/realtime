<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Lancamento extends Model
{

    const REDIS_KEY = 'lancamentos';

    protected $table = self::REDIS_KEY;

    protected $guarded = ['id'];


}
