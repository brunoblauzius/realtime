<?php

namespace App\Events;

use App\Model\Lancamento;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class LancamentosSend implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;



    public $lancamento;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Lancamento $lancamento)
    {
        $this->lancamento = $lancamento;
        $this->dontBroadcastToCurrentUser();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('lancamentos');
    }
}
