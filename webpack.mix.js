const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .copy('resources/js/socket.io.js', 'public/js/socket.io.js')
    .copy('node_modules/bootstrap/dist/js/bootstrap.js', 'public/js/bootstrap/bootstrap.js')
    .copy('node_modules/bootstrap/dist/css/', 'public/css/bootstrap/')
    .sass('resources/sass/app.scss', 'public/css');
