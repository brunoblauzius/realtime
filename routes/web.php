<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Broadcast;

Route::get('/', function () {
    return view('welcome');
});

Broadcast::routes();

Route::get('/start', function (){
    //event(new \App\Events\MessageSendSocket("Teste de envio de mensagem 3"));
    event(new \App\Events\LancamentosSend());
    return "ok";
});


Route::get('/show', 'Lancamento\LancamentosController@lists');

